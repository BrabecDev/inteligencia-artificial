# Repositório Inteligência Artificial

### Autor: BrabecDev

## Porque

### O repo será o armazenamento dos meus projetos sobre IA e assuntos relacionados, chatterbots, machine learning.
### O material de apoio para os experimentos estão todos na minha biblioteca de livros do packtpub.


# Material de Apoio e Estudos
### Inteligência Artificial por Hendrik Macedo (UFS-2018.2)
https://drive.google.com/drive/folders/1njahu8j-k3nVtLYcCuXNxkHLa3ta9uyu
### Artigos publicados por Hendrik Macedo
http://www.saense.com.br/autores/artigos-publicados-por-hendrik-macedo/

# Projetos

## helloWord - A Slack Bot Project
### Descrição: Robô slack para auxílio de tarefas nos canais do tisema.
### Material de apoio: 'Building Slack Bots by Paul Asjes'
### Basta executar no terminal de sua escolha dentro do diretório 'Slack Bot' o seguinte comando:
> npm install
### OBSERVAÇÃO: Lembre-se de colocar a token gerada na página de integração de Bots em index.js onde está 'const token...'.

### Possíveis problemas e soluções:
### "TypeError: MemoryDataStore is not a constructor" when "dataStore: new MemoryDataStore() is called"
### https://github.com/PacktPublishing/Building-Slack-Bots/issues/4
### Como o projeto foi realizado em setembro de 2018, a versão do @slack/client é a 4.6.0. Portanto o código utilizado para a execução do robô foi encontrado
### no site da API do Slack:
### https://github.com/slackapi/node-slack-sdk/tree/v4.0.1#posting-a-message-with-the-real-time-messaging-api

### error message:on_event(event: Err(JsonDecode(MissingFieldError("type"))), raw_json: "{"ok":false,"reply_to":0,"error":{"code":0,"msg":"invalid channel id"}}")
### https://github.com/slack-rs/slack-rs/issues/35
### É necessário que você integre o robô na página https://tisema.slack.com/apps/new/A0F7YS25R-bots e o adicione em algum canal do slack antes de executar o comando
> node index.js
